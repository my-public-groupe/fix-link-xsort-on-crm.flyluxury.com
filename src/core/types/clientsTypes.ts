export interface IClients {
  id?: number;
  name: string;
  surname: string;
  email: string;
  phone: string;
}
