export interface IPriceQuote {
  id?: number;
  reservation_dump: string;
  selling_price: number;
  net_price: number;
  profit: number;
  internal_remarks: string;
  selected?: boolean;
  token: string;
}
