export interface IClient {
  id?: number;
  name: string;
  surname: string;
  email: string;
  phone: string;
  num_of_leads?: number;
  additional_emails: string[];
  additional_phones: string[];
}
