import type { IAgent } from "./agentTypes";
import type { IPax } from "./bookingTypes";
import type {
  IBillingDetails,
  IAdditionalServices,
  ICard,
} from "./bookingTypes";
import type { ILead } from "./leadTypes";
import type { IPriceQuote } from "./priceQuoteTypes";

export interface ISale {
  id?: number;
  agent: string;
  supervisor: string;
  status: number;
  lead?: ILead;
  paxes: Array<IPax>;
  price_quote?: IPriceQuote;
  type: number;
  second_agent_id?: number;
  agent_profit?: number | null;
  second_agent_profit?: number | null;
  billing_details: IBillingDetails;
  additional_services: IAdditionalServices;
  card: ICard;
  tips: number | null;
  rating: number | null;
  ticket_protection: boolean;
  contact_email: string;
  contact_phone: string;
  additional_email: string;
  additional_phone: string;
  client_ip: string | null;
  second_agent: IAgent;
  airline_confirmations: string[] | null;
}

export interface ISaleRow {
  id?: number;
  agent: string;
  created_at: string;
  pax_names: string;
  miles_amount: number;
  airline_code: string;
  gds_pnr: string;
  airline_pnr: string;
  ticket_number: string;
  selling_price: number;
  net_price: number;
  total_profit: number;
  type_of_fare: string;
  commission_amount: number;
  taxes: number;
  ck_fees: number;
  fop: string;
  issuing_fees: number;
  pcc: string;
  status: number;
}

export interface ISaleDetail {
  id?: number;
  type: number;
  airline_code: string;
  gds_pnr: string;
  airline_pnr: string;
  ticket_number: string;
  selling_price: number;
  net_price: number;
  total_profit: number;
  fare: number;
  commission_amount: number;
  ck_fees: number;
  fop: string;
  issuing_fees: number;
  pcc: string;
  taxes: number;
  price_per_mile: number;
  miles_amount: number;
  tips: number;
  ticket_protection: boolean;
  rating: number;
  pax_id: number;
}
