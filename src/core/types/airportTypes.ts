export interface IAirport {
  id: string;
  name: string;
}

export interface IAirline {
  id: string;
  name: string;
}
