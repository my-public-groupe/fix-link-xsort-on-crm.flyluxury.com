import type { IAgent } from "./agentTypes";
import type { IClient } from "./clientTypes";

export interface ILead {
  id: number;
  number: string;
  created_at: string;
  client: string;
  agent: string;
  request_details_text: string;
  status: string;
}

export interface ILeadDetails {
  id?: number;
  number?: string;
  agent_id?: number;
  client_id?: number;
  comments: string;
  status?: number;
  client_type: number;
  request_details: Array<IRequestDetails>;
}

export interface IRequestDetails {
  id?: number;
  airport_from_code: string;
  airport_to_code: string;
  date: string;
  cabin_class?: number;
  adults?: number;
  children?: number;
  infants?: number;
}

export interface ILeadDetailsFull {
  id: number;
  number: string;
  created_at: string;
  client: IClient;
  agent: IAgent;
  status_id: number;
  status: string;
  status_color: string;
  cabin_class_id: number;
  cabin_class: string;
  client_type_id: number;
  client_type: string;
  comments: string;
  request_details?: Array<IRequestDetails>;
  request_details_text?: string;
}

export interface IAdditionalInfo {
  id: number;
  pcc: string;
  pnr: string;
  transaction_id: string;
  mileage_type: string;
  total_miles: number;
  total_amount: number;
}
