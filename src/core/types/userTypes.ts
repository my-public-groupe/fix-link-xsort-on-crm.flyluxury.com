export interface IUser {
  id?: number;
  created_at?: string;
  name: string;
  email: string;
  supervisor_id: number;
  enabled: boolean;
  role: string;
  phone: string;
}
