export interface ICard {
  card_number: string;
  month: string;
  year: string | null;
  cvv: string;
  first_name: string;
  last_name: string;
  valid_thru: string;
  owner: string;
}

export interface IPax {
  gender: "m" | "f";
  first_name: string;
  last_name: string;
  middle_name: string;
  birthday: string;
  email: string;
  phone: string;
  id?: number;
}

export interface IPaxMin {
  gender: "m" | "f";
  first_name: string;
  last_name: string;
  middle_name: string;
  birthday: string;
}

export interface IBillingDetails {
  country: string;
  zip?: number;
  state: string;
  city: string;
  email: string;
  phone: string;
  street_address: string;
}

export interface IPriceQuote {
  id: number;
  uuid: string;
  reservations: IReservation[];
  price: number;
  airline: IAirline;
  active: boolean;
}
export interface IReservation {
  index: number;
  airport_from: IAirport;
  airport_to: IAirport;
  date_from: IFlightDate;
  date_to: IFlightDate;
  travel_time: string;
  airline: IAirline;
  class_name: string;
  layover_time?: string;
  flight_number: string;
  operated_by: string | null;
}

interface IAirport {
  code: string;
  name: string;
  timezone: string;
  city: string;
}

interface IFlightDate {
  date: string;
  time: string;
}

interface IAirline {
  code: string;
  name: string;
  logo: string;
}

export interface IAdditionalServices {
  seat_preference: string;
  meal_preference: string;
  special_assistance: string;
  frequent_flyer: string;
  program_number: string;
}

export interface IClient {
  email: string;
  phone: string;
  contact_email: string;
  contact_phone: string;
  additional_email: string;
  additional_phone: string;
}

export interface IAgentDetails {
  name: string;
  email: string;
  phone: string;
}

export interface IIpDetails {
  ip: string;
}
