import { Modal } from "bootstrap";

const hideModal = (modalEl: HTMLElement | null): void => {
  if (!modalEl) {
    return;
  }

  const myModal = Modal.getInstance(modalEl);
  myModal?.hide();
};

const onShowModal = (modalEl: HTMLElement | null, callback: Function): void => {
  if (!modalEl) {
    return;
  }

  modalEl.addEventListener("shown.bs.modal", function () {
    callback();
  });
};

const onHideModal = (modalEl: HTMLElement | null, callback: Function): void => {
  if (!modalEl) {
    return;
  }

  modalEl.addEventListener("hidden.bs.modal", function () {
    callback();
  });
};

const removeModalBackdrop = (): void => {
  if (document.querySelectorAll(".modal-backdrop.fade.show").length) {
    document.querySelectorAll(".modal-backdrop.fade.show").forEach((item) => {
      item.remove();
    });
  }
};

export { removeModalBackdrop, hideModal, onShowModal, onHideModal };
