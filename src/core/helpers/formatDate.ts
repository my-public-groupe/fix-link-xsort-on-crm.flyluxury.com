import moment from "moment";

export const formatDate = (date: string) => {
  let formattedDate = moment(date, "ddd, D MMM").format("dddd, D MMMM YYYY");
  let year = moment().year(); // Получаем текущий год

  for (let i = 0; i < 3; i++) {
    const testDate = moment(`${date} ${year}`, "ddd, D MMM YYYY");
    if (testDate.isValid()) {
      formattedDate = testDate.format("dddd, D MMMM YYYY");
      return formattedDate;
    } else {
      year++;
    }
  }

  return date;
};
