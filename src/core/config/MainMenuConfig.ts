export interface MenuItem {
  heading?: string;
  sectionTitle?: string;
  route?: string;
  pages?: Array<MenuItem>;
  svgIcon?: string;
  fontIcon?: string;
  sub?: Array<MenuItem>;
  permission?: string;
}

const MainMenuConfig: Array<MenuItem> = [
  {
    pages: [
      {
        heading: "Leads",
        route: "/leads",
        svgIcon: "/media/icons/duotune/art/art002.svg",
        fontIcon: "bi-app-indicator",
        permission: "edit leads",
      },
      {
        heading: "Users",
        route: "/users",
        svgIcon: "/media/icons/duotune/communication/com014.svg",
        fontIcon: "bi-layers",
        permission: "edit leads",
      },
      {
        heading: "Clients",
        route: "/clients",
        svgIcon: "/media/icons/duotune/communication/com005.svg",
        fontIcon: "bi-layers",
        permission:
          "edit leads, edit sales, edit users, edit report, show unassigned, change lead agents, change sale status",
      },
      {
        heading: "Sales",
        route: "/sales",
        svgIcon: "/media/icons/duotune/graphs/gra010.svg",
        fontIcon: "bi-layers",
        permission: "edit sales",
      },
    ],
  },
];

export default MainMenuConfig;
