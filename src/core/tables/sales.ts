import { ref } from "vue";

export const revenueSaleHeader = ref([
  {
    columnName: "Airline",
    columnLabel: "airline_code",
    sortEnabled: false,
  },
  {
    columnName: "GDS PNR",
    columnLabel: "gds_prn",
    sortEnabled: false,
  },
  {
    columnName: "Airline PNR",
    columnLabel: "airline_prn",
    sortEnabled: false,
  },
  {
    columnName: "Ticket number",
    columnLabel: "ticket_number",
    sortEnabled: false,
  },
  {
    columnName: "Selling Price",
    columnLabel: "selling_price",
    sortEnabled: false,
  },
  {
    columnName: "Net Price",
    columnLabel: "net_price",
    sortEnabled: false,
  },
  {
    columnName: "Total Profit",
    columnLabel: "total_profit",
    sortEnabled: false,
  },
  {
    columnName: "Type of fare",
    columnLabel: "type_of_fare",
    sortEnabled: false,
  },
  {
    columnName: "Commission Amount",
    columnLabel: "commission_amount",
    sortEnabled: false,
  },
  {
    columnName: "CK fees",
    columnLabel: "ck_fees",
    sortEnabled: false,
  },
  {
    columnName: "FOP",
    columnLabel: "fop",
    sortEnabled: false,
  },
  {
    columnName: "Issuing fees",
    columnLabel: "issuing_fees",
    sortEnabled: false,
  },
  {
    columnName: "PCC",
    columnLabel: "pcc",
    sortEnabled: false,
  },
  {
    columnName: "",
    columnLabel: "adjust",
    sortEnabled: false,
  },
]);

export const awardSaleHeader = ref([
  {
    columnName: "Airline PNR",
    columnLabel: "airline_pnr",
    sortEnabled: false,
  },
  {
    columnName: "Ticket number",
    columnLabel: "ticket_number",
    sortEnabled: false,
  },
  {
    columnName: "Selling Price",
    columnLabel: "selling_price",
    sortEnabled: false,
  },
  {
    columnName: "Net Price",
    columnLabel: "net_price",
    sortEnabled: false,
  },
  {
    columnName: "Total Profit",
    columnLabel: "total_profit",
    sortEnabled: false,
  },
  {
    columnName: "Taxes",
    columnLabel: "taxes",
    sortEnabled: false,
  },
  {
    columnName: "Price per mile",
    columnLabel: "price_per_mile",
    sortEnabled: false,
  },
  {
    columnName: "Miles amount",
    columnLabel: "miles_amount",
    sortEnabled: false,
  },
  {
    columnName: "CK fees",
    columnLabel: "ck_fees",
    sortEnabled: false,
  },
  {
    columnName: "FOP",
    columnLabel: "fop",
    sortEnabled: false,
  },
  {
    columnName: "",
    columnLabel: "adjust",
    sortEnabled: false,
  },
]);

export const salesListHeader = ref([
  {
    columnName: "Sale ID",
    columnLabel: "number",
  },
  {
    columnName: "Agent",
    columnLabel: "agent",
  },
  {
    columnName: "Created at",
    columnLabel: "created_at",
  },
  {
    columnName: "Pax Names",
    columnLabel: "pax_names",
  },
  {
    columnName: "Total Miles",
    columnLabel: "miles_amount",
  },
  {
    columnName: "GDS PNR",
    columnLabel: "gds_pnr",
    sortEnabled: false,
  },
  {
    columnName: "Airline PNR",
    columnLabel: "airline_pnr",
    sortEnabled: false,
  },
  {
    columnName: "Selling Price",
    columnLabel: "selling_price",
    sortEnabled: false,
  },
  {
    columnName: "Net Price",
    columnLabel: "net_price",
    sortEnabled: false,
  },
  {
    columnName: "Final Profit",
    columnLabel: "final_profit",
    sortEnabled: false,
  },
]);
