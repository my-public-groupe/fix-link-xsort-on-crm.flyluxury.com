import { useAuthStore } from "@/stores/auth";

const can = (el, binding, vnode) => {
  const store = useAuthStore();
  if (store.user.is_superadmin) {
    return (vnode.el.hidden = false);
  }
  const permissions = store.permissions;
  if (permissions.includes(binding.value)) {
    return (vnode.el.hidden = false);
  } else {
    return (vnode.el.hidden = true);
  }
};

export { can };
