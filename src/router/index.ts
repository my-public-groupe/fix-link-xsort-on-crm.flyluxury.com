import {
  createRouter,
  createWebHistory,
  type RouteRecordRaw,
} from "vue-router";
import { useAuthStore } from "@/stores/auth";
import { useConfigStore } from "@/stores/config";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    redirect: "/leads",
    component: () => import("@/layouts/main-layout/MainLayout.vue"),
    meta: {
      middleware: "auth",
    },
    children: [
      {
        path: "/leads",
        name: "leads",
        component: () => import("@/views/leads/LeadsList.vue"),
        meta: {
          pageTitle: "Leads",
          breadcrumbs: ["Leads"],
        },
      },
      {
        path: "/leads/create",
        name: "leads-create",
        component: () => import("@/views/leads/LeadsEdit.vue"),
        meta: {
          pageTitle: "Create lead",
          breadcrumbs: ["Leads", "Create lead"],
        },
      },
      {
        path: "/leads/:number",
        name: "leads-view",
        component: () => import("@/views/leads/LeadsView.vue"),
        meta: {
          pageTitle: "Lead Details",
          breadcrumbs: ["Leads", "Lead details"],
        },
      },
      {
        path: "/leads/:number/edit",
        name: "leads-edit",
        component: () => import("@/views/leads/LeadsEdit.vue"),
        meta: {
          pageTitle: "Edit lead",
          breadcrumbs: ["Leads", "Edit lead"],
        },
      },
      {
        path: "/leads/client-requests",
        name: "leads-client-requests",
        component: () => import("@/views/leads/LeadsClientRequests.vue"),
        meta: {
          pageTitle: "Client Requests",
          breadcrumbs: ["Leads", "Client Requests"],
        },
      },
      {
        path: "/users",
        name: "users",
        component: () => import("@/views/users/UsersList.vue"),
        meta: {
          pageTitle: "Users",
          breadcrumbs: ["Users"],
        },
      },
      {
        path: "/clients",
        name: "clients",
        component: () => import("@/views/clients/ClientsList.vue"),
        meta: {
          pageTitle: "Clients",
          breadcrumbs: ["Clients"],
        },
      },
      {
        path: "/sales",
        name: "sales",
        component: () => import("@/views/sales/SalesList.vue"),
        meta: {
          pageTitle: "Sales",
          breadcrumbs: ["Sales"],
        },
      },
      {
        path: "/sales/:id/edit",
        name: "sales-edit",
        component: () => import("@/views/sales/SalesEdit.vue"),
        meta: {
          pageTitle: "Edit sale",
          breadcrumbs: ["Sales", "Edit sale"],
        },
      },
    ],
  },
  {
    path: "/",
    component: () => import("@/layouts/AuthLayout.vue"),
    children: [
      {
        path: "/sign-in",
        name: "sign-in",
        component: () => import("@/views/authentication/basic-flow/SignIn.vue"),
        meta: {
          pageTitle: "Sign In",
        },
      },
      {
        path: "/sign-up",
        name: "sign-up",
        component: () => import("@/views/authentication/basic-flow/SignUp.vue"),
        meta: {
          pageTitle: "Sign Up",
        },
      },
      {
        path: "/forgot-password",
        name: "forgot-password",
        component: () =>
          import("@/views/authentication/basic-flow/ForgotPassword.vue"),
        meta: {
          pageTitle: "Password reset",
        },
      },
      {
        path: "/password-reset",
        name: "password-reset",
        component: () =>
          import("@/views/authentication/basic-flow/PasswordReset.vue"),
        meta: {
          pageTitle: "Password reset",
        },
      },
    ],
  },
  {
    path: "/",
    component: () => import("@/layouts/SystemLayout.vue"),
    children: [
      {
        // the 404 route, when none of the above matches
        path: "/404",
        name: "404",
        component: () => import("@/views/authentication/Error404.vue"),
        meta: {
          pageTitle: "Error 404",
        },
      },
      {
        path: "/500",
        name: "500",
        component: () => import("@/views/authentication/Error500.vue"),
        meta: {
          pageTitle: "Error 500",
        },
      },
    ],
  },
  {
    path: "/booking/:token",
    component: () => import("@/views/booking/BookingPage.vue"),
    meta: {
      pageTitle: "Booking page",
    },
  },
  {
    path: "/booking/card/:token",
    component: () => import("@/views/booking/BookingCardPage.vue"),
    meta: {
      pageTitle: "Booking page",
    },
  },
  {
    path: "/booking/confirmation/:name",
    component: () => import("@/views/booking/ConfirmationPage.vue"),
    meta: {
      pageTitle: "Thank you for your purchase",
    },
  },
  {
    path: "/booking/demo/:id/:token",
    component: () => import("@/views/booking/BookingDemoPage.vue"),
    meta: {
      pageTitle: "Booking page",
      middleware: "auth",
    },
  },
  {
    path: "/:pathMatch(.*)*",
    redirect: "/404",
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  const authStore = useAuthStore();
  const configStore = useConfigStore();

  // current page view title
  document.title = `${to.meta.pageTitle} - ${import.meta.env.VITE_APP_NAME}`;

  // reset config to initial state
  configStore.resetLayoutConfig();

  // verify auth token before each page change
  authStore.verifyAuth();

  // before page access check if page requires authentication
  if (to.meta.middleware == "auth") {
    if (authStore.isAuthenticated) {
      next();
    } else {
      next({ name: "sign-in" });
    }
  } else {
    next();
  }

  // Scroll page to top on every route change
  window.scrollTo({
    top: 0,
    left: 0,
    behavior: "smooth",
  });
});

export default router;
