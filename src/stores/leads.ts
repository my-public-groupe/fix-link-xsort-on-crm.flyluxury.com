import ApiService from "@/core/services/ApiService";
import ErrorService from "@/core/services/ErrorService";
import type {
  ILead,
  ILeadDetails,
  IAdditionalInfo,
} from "@/core/types/leadTypes";
import { defineStore } from "pinia";
import { ref } from "vue";

export interface DTMeta {
  current_page: number;
  total: number;
}

export const useLeadStore = defineStore("leads", () => {
  const leads = ref<Array<ILead>>([]);
  const numUnassignedLeads = ref(0);
  const meta = ref<DTMeta>();

  async function getLeads(params: any) {
    const response = await ApiService.get("leads", params);
    leads.value = response.data.data;
    meta.value = response.data.meta;

    return leads.value;
  }

  async function getUnassignedLeads() {
    const response = await ApiService.get("leads/unassigned");
    numUnassignedLeads.value = response.data.data;
  }

  async function takeLead() {
    await ApiService.get("leads/take");
  }

  async function saveLead(lead: ILeadDetails) {
    try {
      if (lead.id) {
        const response = await ApiService.put(`leads/${lead.id}`, lead);
        return response.data.data as ILeadDetails;
      } else {
        const response = await ApiService.post("leads", lead);
        return response.data.data.id as ILeadDetails;
      }
    } catch (e) {
      ErrorService.onApiError(e);
    }
  }

  async function deleteLeadDetails(lead: ILeadDetails, requestDetails) {
    try {
      await ApiService.delete(
        `leads/${lead.id}/request-details/${requestDetails}`
      );
    } catch (error) {
      console.error("Error deleting sale detail:", error);
      throw error;
    }
  }

  async function saveAdditionalInfo(info: IAdditionalInfo | undefined) {
    const response = await ApiService.put(`leads/${info?.id}/additional`, info);
    return response.data.data as IAdditionalInfo;
  }

  async function getLead(id: string) {
    const response = await ApiService.get(`leads/${id}`);
    return response.data.data;
  }

  async function getLeadByNumber(number: string) {
    const response = await ApiService.get(`leads/get-by-number/${number}`);
    return response.data.data;
  }

  async function getAdditionalInfo(id: number) {
    const response = await ApiService.get(`leads/${id}/additional`);
    return response.data.data as IAdditionalInfo;
  }

  async function removeLead(number: number) {
    await ApiService.delete(`leads/get-by-number/${number}`);
  }

  return {
    getLeads,
    leads,
    meta,
    getUnassignedLeads,
    numUnassignedLeads,
    takeLead,
    saveLead,
    getLead,
    getLeadByNumber,
    removeLead,
    getAdditionalInfo,
    saveAdditionalInfo,
    deleteLeadDetails,
  };
});
