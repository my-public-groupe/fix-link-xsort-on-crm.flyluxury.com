import ApiService from "@/core/services/ApiService";
import { defineStore } from "pinia";
import { ref } from "vue";
import type { IClient } from "@/core/types/clientTypes";
import ErrorService from "@/core/services/ErrorService";
import { AxiosError } from "axios";

export const useClientsStore = defineStore("clients", () => {
  const clients = ref<Array<IClient>>([]);
  const selectedClientId = ref<number>();

  async function getAllClients() {
    const response = await ApiService.get("clients/list");
    clients.value = response.data.data;

    return clients.value;
  }

  async function saveClient(client: IClient) {
    try {
      if (client.id) {
        const response = await ApiService.put(`clients/${client.id}`, client);
        const updatedClient = response.data.data as IClient;
        selectedClientId.value = updatedClient.id;
      } else {
        const response = await ApiService.post(`clients`, client);
        const newClient = response.data.data as IClient;
        clients.value.push(newClient);
        selectedClientId.value = newClient.id;
      }
    } catch (error) {
      if (
        error instanceof AxiosError &&
        error.response?.data.errors.email_exists
      ) {
        return error;
      } else {
        ErrorService.onApiError(error);
      }
    }
  }

  async function getClientById(clientId: number) {
    const response = await ApiService.get(`clients/${clientId}`);
    return response.data.data;
  }

  async function getClientLeads(clientId: number) {
    try {
      const response = await ApiService.get(`clients/${clientId}/leads`);
      const leads = response.data;

      return leads;
    } catch (error) {
      console.error(error);
    }
  }

  return {
    clients,
    getAllClients,
    saveClient,
    selectedClientId,
    getClientById,
    getClientLeads,
  };
});
