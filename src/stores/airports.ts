import ApiService from "@/core/services/ApiService";
import { defineStore } from "pinia";

export const useAirportStore = defineStore("airports", () => {
  async function getAirports(iata_code: string) {
    const response = await ApiService.get("airports", { iata_code });
    return response.data.data;
  }

  async function getAirlines(iata_code: string) {
    const response = await ApiService.get("airlines", { iata_code });
    return response.data.data;
  }

  async function getCities(iata_code: string) {
    const response = await ApiService.get("cities", { iata_code });
    return response.data.data;
  }

  return {
    getAirports,
    getAirlines,
    getCities,
  };
});
