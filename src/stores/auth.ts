import { ref } from "vue";
import { defineStore } from "pinia";
import ApiService from "@/core/services/ApiService";
import JwtService from "@/core/services/JwtService";

export interface User {
  name: string;
  email: string;
  is_superadmin: boolean;
}

export const useAuthStore = defineStore("auth", () => {
  const errors = ref({});
  const user = ref<User>({} as User);
  const permissions = ref<Array<string>>([]);
  const isAuthenticated = ref(!!JwtService.getToken());

  function setAuth(authUser: User) {
    user.value = authUser;
    errors.value = {};
  }

  function setPermissions(param: Array<string>) {
    permissions.value = param;
    errors.value = {};
  }

  function setToken(token: string) {
    isAuthenticated.value = true;
    JwtService.saveToken(token);
  }

  function setError(error: any) {
    errors.value = { ...error };
  }

  function purgeAuth() {
    isAuthenticated.value = false;
    user.value = {} as User;
    errors.value = [];
    JwtService.destroyToken();
  }

  function getCsrfToken() {
    return ApiService.get("sanctum/csrf-cookie").catch(({ response }) => {
      setError(response.data.errors);
    });
  }

  async function login(credentials: User) {
    await getCsrfToken();

    return ApiService.post("login", credentials)
      .then(({ data }) => {
        setToken(data.api_token);
      })
      .catch(({ response }) => {
        setError(response.data.errors);
      });
  }

  function logout() {
    purgeAuth();
  }

  function register(credentials: User) {
    return ApiService.post("register", credentials)
      .then(() => {
        setError({});
      })
      .catch(({ response }) => {
        setError(response.data.errors);
      });
  }

  function forgotPassword(email: string) {
    return ApiService.post("forgot-password", email)
      .then(() => {
        setError({});
      })
      .catch(({ response }) => {
        setError(response.data.errors);
      });
  }

  function resetPassword(credentials: User, token: string) {
    return ApiService.post("reset-password", { ...credentials, token })
      .then(() => {
        setError({});
      })
      .catch(({ response }) => {
        setError(response.data.errors);
      });
  }

  function verifyAuth() {
    if (JwtService.getToken()) {
      ApiService.setHeader();
      ApiService.post("me", {})
        .then(({ data }) => {
          setAuth(data.user);
          setPermissions(data.permissions);
        })
        .catch(({ response }) => {
          setError(response.data.errors);
          purgeAuth();
        });
    } else {
      purgeAuth();
    }
  }

  function checkPermission(permission: string): boolean {
    if (user.value.is_superadmin) {
      return true;
    }
    return permissions.value.includes(permission);
  }

  function isSuperadmin(): boolean {
    return user.value.is_superadmin;
  }

  return {
    errors,
    user,
    isAuthenticated,
    permissions,
    login,
    logout,
    register,
    forgotPassword,
    resetPassword,
    verifyAuth,
    checkPermission,
    isSuperadmin,
  };
});
