/* eslint-disable no-empty */
import ApiService from "@/core/services/ApiService";
import ErrorService from "@/core/services/ErrorService";
import type { ISale, ISaleDetail, ISaleRow } from "@/core/types/saleTypes";
import { defineStore } from "pinia";
import { reactive, ref } from "vue";

interface AppState {
  isButtonClicked: boolean;
}

export interface StoreDefinition {
  sales: ISale[];
}

const state = reactive<AppState>({
  isButtonClicked: false,
});

const mutations = {
  setButtonClicked(state: AppState, isClicked: boolean) {
    state.isButtonClicked = isClicked;
  },
};

export default {
  state,
  mutations,
};

export interface DTMeta {
  current_page: number;
  total: number;
}

export const useSaleStore = defineStore("sales", () => {
  const sales = ref<Array<ISaleRow>>([]);
  const meta = ref<DTMeta>();
  const sale = ref<Array<ISale>>([]);
  const saleDetails = ref<Array<ISaleDetail>>([]);

  function setSales(data: Array<ISaleRow>) {
    sales.value = data;
  }

  async function getSales(params: any) {
    const response = await ApiService.get("sales", params);
    sales.value = response.data.data;
    meta.value = response.data.meta;
  }

  async function createSale(price_quote_id: number) {
    const response = await ApiService.post(`sales`, { price_quote_id });
    return response.data.sale_id as number;
  }

  async function saveSale(sale: ISale) {
    try {
      if (sale.id) {
        const response = await ApiService.put(`sales/${sale.id}`, sale);
        return response.data.data as ISale;
      }
    } catch (e) {
      ErrorService.onApiError(e);
    }
  }

  async function getSale(id: string) {
    const response = await ApiService.get(`sales/${id}`);
    return response.data.data as ISale;
  }

  async function getSaleDetails(id: number) {
    const response = await ApiService.get(`sales/${id}/details`);
    saleDetails.value = response.data.data;
  }

  async function saveSaleDetail(saleDetail: ISaleDetail, saleId: number) {
    try {
      if (saleDetail.id) {
        await ApiService.put(
          `/sales/${saleId}/details/${saleDetail.id}`,
          saleDetail
        );
        const index = saleDetails.value.findIndex(
          (detail) => detail.id === saleDetail.id
        );
        if (index !== -1) {
          saleDetails.value[index] = saleDetail;
        }
      } else {
        const response = await ApiService.post(
          `/sales/${saleId}/details`,
          saleDetail
        );
        saleDetails.value.push(response.data.data);
      }
      return true;
    } catch (e) {
      ErrorService.onApiError(e);
    }
  }

  async function addSaleDetail(saleDetail: ISaleDetail, saleId: number) {
    try {
      if (saleDetail.id) {
        const response = await ApiService.post(
          `/sales/${saleId}/details`,
          saleDetail
        );
        saleDetails.value.push(response.data.data);
      }
      return true;
    } catch (e) {
      ErrorService.onApiError(e);
    }
  }

  async function deleteSaleDetail(saleDetail: ISaleDetail, saleId: number) {
    try {
      await ApiService.delete(`/sales/${saleId}/details/${saleDetail.id}`);

      const index = saleDetails.value.findIndex(
        (detail) => detail.id === saleDetail.id
      );
      if (index !== -1) {
        saleDetails.value.splice(index, 1);
      }
    } catch (error) {
      console.error("Error deleting sale detail:", error);
      throw error;
    }
  }

  async function removeSale(id: number) {
    await ApiService.delete(`sales/${id}`);
  }

  async function sendTicket(saleId: number, contactEmails: string[]) {
    try {
      const data = { emails: contactEmails };
      await ApiService.post(`sales/${saleId}/send-ticket`, data);
      return true;
    } catch (error) {
      console.error("Error sending ticket:", error);
      return false;
    }
  }

  async function previewTicket(sale: ISale) {
    const response = await ApiService.post(
      `/sales/${sale.id}/preview-ticket`,
      sale
    );
    return response.data;
  }

  return {
    getSales,
    setSales,
    sales,
    sale,
    meta,
    createSale,
    saveSale,
    getSale,
    removeSale,
    getSaleDetails,
    saveSaleDetail,
    saleDetails,
    sendTicket,
    deleteSaleDetail,
    addSaleDetail,
    previewTicket,
  };
});
