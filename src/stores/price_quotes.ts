import ApiService from "@/core/services/ApiService";
import type { IPriceQuote } from "@/core/types/priceQuoteTypes";
import { defineStore } from "pinia";
import { ref } from "vue";
import ErrorService from "@/core/services/ErrorService";

export const usePriceQuoteStore = defineStore("price_quotes", () => {
  const priceQuotes = ref<Array<IPriceQuote>>([]);

  async function savePriceQuote(priceQuote: IPriceQuote, lead_id: number) {
    try {
      if (priceQuote.id) {
        // TODO
      } else {
        await ApiService.post(`leads/${lead_id}/price-quotes`, priceQuote);
        await getPriceQuotes(lead_id);
        return true;
      }
    } catch (error) {
      ErrorService.onApiError(error);
    }
  }

  async function getPriceQuotes(lead_id: number) {
    const response = await ApiService.get(`leads/${lead_id}/price-quotes`);
    priceQuotes.value = response.data.data;
  }

  async function sendPriceQuotes(price_quotes: Array<number>) {
    await ApiService.post("price-quotes/send-notification", price_quotes);
  }

  async function previewEmail(price_quotes: Array<number>) {
    const response = await ApiService.post(
      "price-quotes/preview-email",
      price_quotes
    );
    return response.data;
  }

  async function removePriceQuote(lead_id: number, id: number) {
    await ApiService.delete(`leads/${lead_id}/price-quotes/${id}`);
    priceQuotes.value = priceQuotes.value.filter((obj) => obj.id !== id);
  }

  return {
    priceQuotes,
    savePriceQuote,
    getPriceQuotes,
    sendPriceQuotes,
    removePriceQuote,
    previewEmail,
  };
});
