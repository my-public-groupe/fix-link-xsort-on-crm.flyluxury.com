import ApiService from "@/core/services/ApiService";
import type {
  ICard,
  IPax,
  IReservation,
  IBillingDetails,
  IAdditionalServices,
  IPriceQuote,
  IClient,
} from "@/core/types/bookingTypes";
import { defineStore } from "pinia";
import { ref } from "vue";
import ErrorService from "@/core/services/ErrorService";

export const useBookingStore = defineStore("booking", () => {
  const card = ref<ICard>();
  const pax = ref<IPax>();
  const billing = ref<IBillingDetails>();
  const additional = ref<IAdditionalServices>();
  const client = ref<IClient>();

  async function getReservationByToken(token: string) {
    const response = await ApiService.get(`booking/${token}`);
    return response.data.current_price_quote as Array<IReservation>;
  }

  async function getPriceQuote(token: string) {
    const uuid = token;
    const id = 1;
    try {
      const response = await ApiService.get(`booking/${uuid}`);
      const reservations = response.data
        .current_price_quote as Array<IReservation>;
      const price = response.data.selling_price;
      const airline = reservations[0].airline;
      return {
        id,
        uuid,
        reservations,
        price,
        airline,
      };
    } catch (error) {
      console.error(`Error fetching quotes`);
    }
  }

  async function storeBookingData(
    token: string,
    cardDetails: ICard | undefined,
    paxesDetails: IPax[] | undefined,
    billingDetails: IBillingDetails | undefined,
    additionalServices: IAdditionalServices | null,
    tipsAmount: number,
    ratingScore: number | undefined,
    ticketProtection: boolean | undefined,
    contactEmail: string,
    contactPhone: string,
    additionalEmail: string,
    additionalPhone: string
  ) {
    try {
      if (!cardDetails && !paxesDetails && !billingDetails) {
        throw new Error("Error");
      }

      const params: {
        card?: ICard;
        paxes?: IPax[];
        billing_details?: IBillingDetails;
        additional_services?: IAdditionalServices | null;
        tips?: number;
        rating?: number;
        ticket_protection?: boolean;
        contact_email?: string;
        contact_phone?: string;
        additional_email?: string;
        additional_phone?: string;
      } = {};

      if (cardDetails) {
        params.card = cardDetails;
      }

      if (paxesDetails) {
        params.paxes = paxesDetails;
      }

      if (billingDetails) {
        params.billing_details = billingDetails;
      }

      params.additional_services = additionalServices;

      if (tipsAmount >= 0) {
        params.tips = tipsAmount;
      }

      params.rating = ratingScore;

      params.ticket_protection = ticketProtection;

      params.contact_email = contactEmail;

      params.contact_phone = contactPhone;

      params.additional_email = additionalEmail;

      params.additional_phone = additionalPhone;

      await ApiService.post(`booking/${token}`, params);
      return true;
    } catch (error) {
      ErrorService.onBookingError(error);
      return false;
    }
  }

  async function sendTicket(token: string) {
    await ApiService.get(`booking/${token}/send-ticket`);
    return true;
  }

  async function getAllPriceQuotes(tokens: string[]) {
    const allResponses: IPriceQuote[] = [];

    for (let i = 0; i < tokens.length; i++) {
      const uuid = tokens[i];
      const id = i + 1;
      try {
        if (uuid) {
          const response = await ApiService.get(`booking/${uuid}`);
          const reservations = response.data
            .current_price_quote as Array<IReservation>;
          const price = response.data.selling_price;
          const airline = reservations[0].airline;
          allResponses.push({
            id,
            uuid,
            reservations,
            price,
            airline,
            active: i === 0,
          });
        }
      } catch (error) {
        console.error(`Error fetching quotes`);
      }
    }

    return allResponses;
  }

  async function getAirlines(search: string) {
    const response = await ApiService.get("booking/airlines", { search });
    return response.data.data;
  }

  async function getClient(token: string) {
    const response = await ApiService.get(`booking/${token}`);
    return response.data.client;
  }

  async function getAgent(token: string) {
    const response = await ApiService.get(`booking/${token}`);
    return response.data.agent;
  }

  async function getIp(token: string) {
    const response = await ApiService.get(`booking/${token}`);
    return response.data.ip;
  }

  return {
    storeBookingData,
    getReservationByToken,
    card,
    pax,
    billing,
    additional,
    sendTicket,
    getAllPriceQuotes,
    getAirlines,
    client,
    getClient,
    getAgent,
    getIp,
    getPriceQuote,
  };
});
