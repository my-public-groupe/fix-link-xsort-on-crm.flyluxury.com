import ApiService from "@/core/services/ApiService";
import type { IUser } from "@/core/types/userTypes";
import { defineStore } from "pinia";
import { ref } from "vue";

export const useUsersStore = defineStore("users", () => {
  const users = ref<Array<IUser>>([]);

  function setUsers(data: Array<IUser>) {
    users.value = data;
  }

  async function getUsers() {
    const response = await ApiService.get("users");
    setUsers(response.data.data);
  }

  async function getSupervisors() {
    const response = await ApiService.get("users/supervisors");
    return response.data.data as Array<IUser>;
  }

  async function saveUser(user: IUser) {
    await ApiService.put(`users/${user.id}`, user);
  }

  return {
    users,
    getUsers,
    getSupervisors,
    saveUser,
  };
});
