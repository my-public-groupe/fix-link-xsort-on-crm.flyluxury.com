import ApiService from "@/core/services/ApiService";
import type { IAgent } from "@/core/types/agentTypes";
import { defineStore } from "pinia";
import { ref } from "vue";

export const useAgentsStore = defineStore("agents", () => {
  const agents = ref<Array<IAgent>>([]);

  async function getAllAgents() {
    const response = await ApiService.get("agents/list");
    agents.value = response.data.data;
  }

  async function getSupervisorAgents() {
    const response = await ApiService.get("supervisor/agents/list");
    agents.value = response.data.data;
  }

  return {
    agents,
    getAllAgents,
    getSupervisorAgents,
  };
});
